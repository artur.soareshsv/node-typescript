export interface Company {
    _id: String;
    name: String;
    cnpj: String;
}