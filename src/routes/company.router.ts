import { Router } from 'express';

import { CompanyController } from '../controllers/company.controller';

export class CompanyRouter {
    router: Router;
    companyController: CompanyController;

    constructor() {
        this.router = Router();
        this.companyController = new CompanyController();
        this.init();
    }

    init(): void {
        this.router.get('/', this.companyController.get);
        this.router.get('/:id', this.companyController.getById);
        this.router.post('/', this.companyController.post);
        this.router.put('/:id', this.companyController.put);
        this.router.delete('/:id', this.companyController.delete);
    }
}

const companyRouter = new CompanyRouter();
export default companyRouter.router;