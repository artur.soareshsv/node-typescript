import { DocumentQuery, Query } from 'mongoose';

import { CompanySchema } from '../schemas/company.schema';
import { Company } from '../models/company';

export class CompanyRepository {
    company: CompanySchema;

    constructor() {
        this.company = new CompanySchema();
    }

    find(): DocumentQuery<any[], any> {
        return this.company.model.find();
    }

    findById(id: Number): DocumentQuery<any, any> {
        return this.company.model.findById(id);
    }

    save(company: Company): Promise<any> {
        let companyToSave = new this.company.model(company);
        return companyToSave.save();
    }

    update(id: Number, company: Company): Query<any> {
        return this.company.model.update({ _id: id }, { $set: company });
    }

    delete(id: Number): Query<any> {
        return this.company.model.deleteOne({ _id: id });
    }
}