import * as mongoose from 'mongoose';

export class CompanySchema {
    model: mongoose.Model<mongoose.Document>;

    constructor() {
        this.model = mongoose.model('company', this.createSchema());
    }

    private createSchema(): mongoose.Schema {
        return new mongoose.Schema({
            name: {
                type: String,
                required: 'Enter the company name'
            },
            cnpj: {
                type: String,
                required: 'Enter the company cnpj'
            }
        });
    }
} 