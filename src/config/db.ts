import * as mongoose from 'mongoose';

export class DB {
    private static url: string = 'mongodb://localhost:27017/nodetypescript'

    constructor() {
    }

    static connect(): void {
        mongoose.connect(this.url, { useNewUrlParser: true });
    }
}