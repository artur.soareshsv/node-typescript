import { Request, Response } from 'express';
import * as redis from 'redis';

import { CompanyRepository } from '../repositories/company.repository';
import { Company } from '../models/company';

export class CompanyController {
    private companyRepository: CompanyRepository;
    private redis: redis.RedisClient;

    constructor() {
        this.companyRepository = new CompanyRepository();
        this.redis = redis.createClient();
    }

    get = (req: Request, res: Response) => {
        this.redis.get('allcompanies', (err, reply) => {
            if (reply) {
                res.send({
                    success: true,
                    data: JSON.parse(reply)
                });
            } else {
                this.companyRepository.find().then((companies: Array<Company>) => {
                    this.redis.setex('allcompanies', 20, JSON.stringify(companies));
                    res.send({
                        success: true,
                        data: companies
                    });
                });
            }
        });

    }

    getById = (req: Request, res: Response) => {
        this.companyRepository.findById(req.params.id).then((company: Company) => {
            res.send({
                success: true,
                data: company
            });
        });
    }

    post = (req: Request, res: Response) => {
        this.companyRepository.save(req.body).then((company: Company) => {
            res.send({
                success: true,
                data: company
            });
        });
    }

    put = (req: Request, res: Response) => {
        this.companyRepository.update(req.params.id, req.body).then(value => {
            res.send({
                success: true,
                data: value
            });
        });
    }

    delete = (req: Request, res: Response) => {
        this.companyRepository.delete(req.params.id).then(value => {
            res.send({
                success: true,
                data: value
            });
        });
    }
}